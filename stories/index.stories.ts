import { html, TemplateResult } from 'lit';
import '../src/dotshaOffer.js';
import '../src/dotshaOfferItem.js';
import '../src/dotshaOffersList.js';

export default {
  title: 'DotshaOffer',
  component: 'dotsha-offer',
  argTypes: {
    title: { control: 'text' },
    counter: { control: 'number' },
    textColor: { control: 'color' },
  },
};

interface Story<T> {
  (args: T): TemplateResult;
  args?: Partial<T>;
  argTypes?: Record<string, unknown>;
}

interface ArgTypes {
  title?: string;
  textColor?: string;
  slot?: TemplateResult;
}

const Template: Story<ArgTypes> = ({
  title = 'Hello world',
  textColor,
  slot,
}: ArgTypes) => html`
  <dotsha-offer
    style="--dotsha-text-color: ${textColor || 'black'}"
    .title=${title}
  >
    ${slot}
  </dotsha-offer>
`;

export const Regular = Template.bind({});

export const CustomTitle = Template.bind({});
CustomTitle.args = {
  title: 'My title',
};

export const SlottedContent = Template.bind({});
SlottedContent.args = {
  slot: html`<p>Slotted content</p>`,
};
SlottedContent.argTypes = {
  slot: { table: { disable: true } },
};
