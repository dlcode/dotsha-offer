const path = require('path');

module.exports = {
  mode: 'production',
  entry: path.resolve(__dirname, 'dist/esm/index.js'),
  devtool: 'nosources-source-map',
  output: {
    // library: 'DotshaOffersList',
    library: {
        name: 'dotsha-offer',
        type: 'global',
    },
    path: path.resolve(__dirname, 'dist'),
    filename: 'dotsha-offer.bundle.js',
  },
  target: ['web', 'es5'],
};
