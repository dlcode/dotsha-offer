/* eslint-disable wc/guard-super-call */
import { html, css, LitElement } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { deserialise } from 'kitsu-core';
import { DotshaSecuredController } from '@dlcode/dotsha-login';

type OfferItem = {
  id: String;
};

type Offer = {
  id: String;
  name: String;
  offerItems: OfferItem[];
};

@customElement('dotsha-offers-list')
export class DotshaOffersList extends LitElement {
  static styles = css`
    :host {
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      color: var(--dotsha-text-color, #000);
    }
    .error {
      width: 400px;
      margin: auto;
    }
    main {
      background: transparent;
    }
    .loading {
      width: 100%;
      height: 100%;
      text-align: center;
      padding-top: 10%;
    }
    main > h2,
    main > h3 {
      display: block;
      text-align: center;
    }
    .offer-list {
      margin-top: 20px;
      display: grid;
      grid-template-columns: repeat(
        auto-fill,
        minmax(
          var(--dotsha-invoice-grid-columns, var(--dotsha-grid-columns, 280px)),
          1fr
        )
      );
      grid-gap: var(--dotsha-invoice-grid-gap, var(--dotsha-grid-gap, 50px));
    }
  `;

  private controller = new DotshaSecuredController(this);

  @state()
  isLoading = true;

  @state()
  error = null;

  @state()
  retry: any = null;

  @state()
  offers: Offer[] = [];

  @state()
  offer: Offer | undefined = undefined;

  @property({ type: String }) title = 'Welcome';

  @property({ type: String, reflect: true }) apiBasePath: String =
    sessionStorage.getItem('dotsha-api-base-path') || '/api';

  connectedCallback() {
    super.connectedCallback();
    window.addEventListener('goto-offers-list', this.goToOffersList);
    window.addEventListener('select-offer', this.selectOffer);
    this.getOffers();
  }

  disconnectedCallback() {
    window.removeEventListener('goto-offers-list', this.goToOffersList);
    window.removeEventListener('select-offer', this.selectOffer);
    super.disconnectedCallback();
  }

  private getOffers = () => {
    this.error = null;
    this.isLoading = true;
    return fetch(`offers.json`, {
      method: 'GET',
      headers: new Headers({
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }),
    })
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        // throw Error('error');
        return response;
      })
      .then(data => data.json())
      .then(deserialise)
      .then(json => {
        this.offers = json.data;
      })
      .catch(error => {
        this.error = error;
        this.retry = this.getOffers;
      })
      .finally(() => {
        this.isLoading = false;
      });
  };

  private goToOffersList = () => {
    this.offer = undefined;
  };

  private selectOffer = (event: any) => {
    this.error = null;
    this.isLoading = true;
    const offerId =
      event.detail.id || event.target.getAttribute('data-offer-id');
    this.offer = this.offers?.find(({ id }) => id === offerId);
    this.isLoading = false;
  };

  renderContent() {
    if (!this.controller.isLoggedIn) {
      return html`<dotsha-restricted></dotsha-restricted>`;
    }

    if (this.error) {
      return html`
        <dotsha-card elevation="1" class="error">
          <h2 slot="header">ERREUR</h2>
          <span slot="content">${this.error}</span>
          <div slot="footer">
            ${this.retry
              ? html`<dotsha-button @click=${this.retry}>
                  Réessayer
                </dotsha-button>`
              : ''}
          </div>
        </dotsha-card>
      `;
    }

    if (this.isLoading) {
      return html`<div class="loading"><dotsha-loading></dotsha-loading></div>`;
    }

    if (this.offers.length) {
      return html`
        <div class="offer-list">
          ${this.offers.map(
            (offer: Offer) => html`
              <dotsha-offer-item .offer=${offer}></dotsha-offer-item>
            `
          )}
        </div>
      `;
    }

    return html`<h3>Rien à voir ici &#x1F648;</h3>`;
  }

  override render() {
    if (this.offer) {
      return html`<dotsha-offer .offer=${this.offer}></dotsha-offer>`;
    }

    return html`
      <dotsha-card>
        <h1 slot="header">${this.title}</h1>
        <div slot="content">${this.renderContent()}</div>
        <div slot="footer"><slot></slot></div>
      </dotsha-card>
    `;
  }
}
