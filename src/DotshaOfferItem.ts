import { html, css, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

@customElement('dotsha-offer-item')
export class DotshaOfferItem extends LitElement {
  static styles = css`
    :host {
      cursor: pointer;
    }
    .dotsha-offer-item {
      display: grid;
      grid-template-rows: max-content;
      box-shadow: 0 4px 8px 0 var(--dotsha-box-shadow-color, rgba(0, 0, 0, 0.2));
      transition: 0.3s;
    }
    .dotsha-offer-item:hover {
      box-shadow: 0 8px 16px 0
        var(--dotsha-box-shadow-color, rgba(0, 0, 0, 0.2));
    }
    .label {
      color: #363636;
      display: block;
      font-size: 1rem;
      font-weight: 700;
      font-family: var(
        --dotsha-label-font,
        --dotsha-font,
        BlinkMacSystemFont,
        -apple-system,
        'Segoe UI',
        'Roboto',
        'Oxygen',
        'Ubuntu',
        'Cantarell',
        'Fira Sans',
        'Droid Sans',
        'Helvetica Neue',
        'Helvetica',
        'Arial',
        sans-serif
      );
    }
  `;

  @property({ type: Object }) offer: any = null;

  private dispatchSelectOffer = () => {
    if (this.offer) {
      this.dispatchEvent(
        new CustomEvent('select-offer', {
          detail: this.offer,
          bubbles: true,
          composed: true,
        })
      );
    }
  };

  override render() {
    return html`
      <dotsha-card
        @click=${this.dispatchSelectOffer}
        @keyup=${this.dispatchSelectOffer}
        class="dotsha-offer-item"
        data-offer-id=${this.offer.id}
      >
        <h4 slot="header"><b>${this.offer?.name || 'Offer'}</b></h4>
        <div slot="content">
          <p>
            <span class="label">Montant</span>
            <span class="number">${this.offer?.price || '-'} €</span>
          </p>
        </div>
      </dotsha-card>
    `;
  }
}
