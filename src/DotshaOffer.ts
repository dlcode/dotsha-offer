import { DotshaSecuredController } from '@dlcode/dotsha-login';
import { html, css, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

@customElement('dotsha-offer')
export class DotshaOffer extends LitElement {
  static styles = css`
    :host {
      font-family: var(
        --dotsha-invoice-font,
        --dotsha-font,
        BlinkMacSystemFont,
        -apple-system,
        'Segoe UI',
        'Roboto',
        'Oxygen',
        'Ubuntu',
        'Cantarell',
        'Fira Sans',
        'Droid Sans',
        'Helvetica Neue',
        'Helvetica',
        'Arial',
        sans-serif
      );
      width: 400px;
      min-height: 480px;
      margin: 50px auto;
    }
    dotsha-card {
      --dotsha-header-font-size: 12px;
      --dotsha-header-line-height: 40px;
      --dotsha-header-font-weight: 700;
    }
    main > h2 {
      display: block;
      text-align: center;
    }
    main > section {
      display: block;
      text-align: center;
      margin-bottom: 1rem;
    }
    .label {
      color: #363636;
      display: block;
      font-size: 1rem;
      font-weight: 700;
      font-family: var(
        --dotsha-label-font,
        --dotsha-font,
        BlinkMacSystemFont,
        -apple-system,
        'Segoe UI',
        'Roboto',
        'Oxygen',
        'Ubuntu',
        'Cantarell',
        'Fira Sans',
        'Droid Sans',
        'Helvetica Neue',
        'Helvetica',
        'Arial',
        sans-serif
      );
    }
    .icon {
      width: 24px;
      height: 24px;
      padding: 0 8px;
    }
    .link {
      border: none;
      cursor: pointer;
      padding: 12px 24px;
      margin: 32px 24px;
      border-radius: 16px;
      display: inline-flex;
      align-items: center;
      text-align: center;
      text-decoration: none;
      font-size: 16px;
    }
    .link,
    .link:visited {
      color: var(--dotsha-link-color, #555);
    }
    footer {
      display: flex;
      flex: 1;
      justify-content: flex-start;
    }
  `;

  private controller = new DotshaSecuredController(this);

  //   @property({ type: Object }) offer: Offer | null = null;
  @property({ type: Object }) offer: any = null;

  @property({ type: Number }) elevation = 1;

  _goToOffersList() {
    this.dispatchEvent(
      new CustomEvent('goto-offers-list', { bubbles: true, composed: true })
    );
  }

  override render() {
    if (!this.controller.isLoggedIn) {
      return html`<dotsha-restricted></dotsha-restricted>`;
    }
    return html`
      <dotsha-card .elevation=${this.elevation}>
        <h1 slot="header">Sélectionner mes options</h1>
        <main slot="content">
          <h2>${this.offer?.name || 'Offer'}</h2>
        </main>
        <footer slot="footer">
          <dotsha-button @click=${this._goToOffersList}> Retour </dotsha-button>
          <dotsha-button class="success">Valider</dotsha-button>
          <slot></slot>
        </footer>
      </dotsha-card>
    `;
  }
}
