import { html } from 'lit';
import { fixture, expect } from '@open-wc/testing';
import { DotshaOffersList } from '../src/DotshaOffersList.js';
import '../src/dotshaOffersList.js';

describe('DotshaOffer', () => {
  it('has a default title "Welcome" and apiBasePath /api', async () => {
    const el = await fixture<DotshaOffersList>(
      html`<dotsha-offers-list></dotsha-offers-list>`
    );

    expect(el.title).to.equal('Welcome');
  });

  it('can override the title via attribute', async () => {
    const el = await fixture<DotshaOffersList>(
      html`<dotsha-offers-list title="attribute title"></dotsha-offers-list>`
    );

    expect(el.title).to.equal('attribute title');
  });

  it('passes the a11y audit', async () => {
    const el = await fixture<DotshaOffersList>(
      html`<dotsha-offers-list></dotsha-offers-list>`
    );

    await expect(el).shadowDom.to.be.accessible();
  });
});
