import { html } from 'lit';
import { fixture, expect, oneEvent } from '@open-wc/testing';
import { DotshaOffer } from '../src/DotshaOffer.js';
import '../src/dotshaOffer.js';

const offer = {
  id: '1234567890',
  name: 'Test',
  offerItems: [
    {
      id: '0123456789',
    },
  ],
};

describe('DotshaOffer', () => {
  it('has a default title "Offer"', async () => {
    const el = await fixture<DotshaOffer>(html`<dotsha-offer></dotsha-offer>`);
    const title = el.shadowRoot!.querySelector('h2')!.textContent;

    expect(title).to.equal('Offer');
  });

  it('Dispatch event on button click', async () => {
    const el = await fixture<DotshaOffer>(html`<dotsha-offer></dotsha-offer>`);
    const listener = oneEvent(el, 'goto-offers-list');

    el.shadowRoot!.querySelector('button')!.click();

    const event = await listener;
    expect(event).to.be.not.null;
  });

  it('can override the title via attribute', async () => {
    const el = await fixture<DotshaOffer>(
      html`<dotsha-offer .offer=${offer}></dotsha-offer>`
    );
    const title = el.shadowRoot!.querySelector('h2')!.textContent;

    expect(title).to.equal(offer.name);
  });

  it('passes the a11y audit', async () => {
    const el = await fixture<DotshaOffer>(html`<dotsha-offer></dotsha-offer>`);

    await expect(el).shadowDom.to.be.accessible();
  });
});
